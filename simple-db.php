<?php
    class SimpleDB {
        protected $database = array();
        protected $transactions = array();
        protected $valueCount = array();

        /**
         * With larger values, we want to hash them for the $valueCount array
         */
        protected $hashValue = true;

        /**
         * Get value of key in database
         */
        public function get($key){
            return isset( $this->database[$key] ) ? $this->database[$key] : null;
        }

        /**
         * Set value of key in database, set value to null to unset
         */
        public function set($key, $value = null){
            $this->createTransaction($key);
            $this->updateValue($key, $value);
        }

        /**
         * Get number of instances of value in database
         */
        public function numEqualTo($value){
            $hash = $this->getValueHash($value);
            return isset( $this->valueCount[$hash] ) ? $this->valueCount[$hash] : 0;
        }

        /**
         * Start new transaction
         */
        public function begin(){
            array_push( $this->transactions, array() );
        }

        /**
         * Rollback all transactions in current instance
         */
        public function rollback(){
            if( !count($this->transactions) ){
                return false;
            }

            $transaction = array_pop($this->transactions);
            foreach($transaction as $key => $value){
                $this->updateValue($key, $value);
            }
            return true;
        }

        /**
         * Commit all transactions, empty transactions array
         */
        public function commit(){
            if( !count($this->transactions) ){
                return false;
            }
            $this->transactions = array();
            return true;
        }

        /**
         * Create transaction item
         */
        protected function createTransaction($key){
            $transaction = end($this->transactions);
            //only set if transaction on $key hasnt happened already
            if( !isset( $transaction[$key] ) ){
                $this->transactions[ key($this->transactions) ][$key] = $this->get($key);
            }
        }

        /**
         * Set value in database, and manage valueCount
         */
        protected function updateValue($key, $value = null){
            if( isset( $this->database[$key] ) ){
                $hash = $this->getValueHash($this->database[$key]);
                $this->valueCount[$hash] -= 1;
                unset($this->database[$key]);
            }

            if($value){
                $this->database[$key] = $value;
                $hash = $this->getValueHash($value);
                $this->valueCount[ $hash ] = isset($this->valueCount[ $hash ]) ? $this->valueCount[ $hash ] + 1 : 1;
            }
        }

        /**
         * Get hashed value for key
         */
        protected function getValueHash($value){
            return $this->hashValue ? md5($value) : $value;
        }
    }

    /**
     * Print out responses from SimpleDB class
     * $forceNull allows output if $output is null
     */
    function handleOutput($output, $forceNull = false){
        $output = $forceNull && is_null($output) ? 'NULL' : $output;
        if( !is_null($output) ){
            fwrite(STDOUT, "> ".$output."\n");
        }
    }
    function handleTransactionOutput($response = true){
        $output = !$response ? "NO TRANSACTION" : null;
        handleOutput($output);
    }

    $simpleDB = new SimpleDB();
    while($line = fgets(STDIN)){
        fwrite(STDOUT, $line);
        $line = str_replace("\n", "", $line);
        $args = explode(" ", $line);
        switch($args[0]){
            case 'SET': $simpleDB->set($args[1], $args[2]);  break;
            case 'GET': handleOutput( $simpleDB->get($args[1]), true ); break;
            case 'UNSET': $simpleDB->set($args[1]);  break;
            case 'NUMEQUALTO': handleOutput( $simpleDB->numEqualTo($args[1]) );  break;
            case 'BEGIN': $simpleDB->begin(); break;
            case 'ROLLBACK': handleTransactionOutput( $simpleDB->rollback() ); break;
            case 'COMMIT': handleTransactionOutput( $simpleDB->commit() ); break;
            case 'END': exit(); break;
            default: exit(); break;
        }
    }
?>